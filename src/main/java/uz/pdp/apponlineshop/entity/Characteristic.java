package uz.pdp.apponlineshop.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.apponlineshop.entity.template.AbsEntity;

import javax.persistence.Entity;

@EqualsAndHashCode
@Data
@Entity
public class Characteristic extends AbsEntity {

}
