package uz.pdp.apponlineshop.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.apponlineshop.entity.Cart;
import uz.pdp.apponlineshop.entity.Product;
import uz.pdp.apponlineshop.entity.User;

import java.util.List;

@Projection(types = Cart.class)
public interface CustomCart {
    Integer getId();

    User getUser();

    List<Product> getProduct();
}
