package uz.pdp.apponlineshop.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.apponlineshop.entity.Characteristic;

@Projection(types = Characteristic.class)
public interface CustomCharacteristic {
    Integer getId();

    String getName();
}
