package uz.pdp.apponlineshop.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.apponlineshop.entity.Order;
import uz.pdp.apponlineshop.entity.OrderType;
import uz.pdp.apponlineshop.entity.Product;
import uz.pdp.apponlineshop.entity.User;

import java.sql.Timestamp;
import java.util.List;

@Projection(types = Order.class)
public interface CustomOrder {
    Integer getId();

    User getUser();

    Double getTotalSum();

    OrderType getOrderType();

    List<Product> getProduct();

    Timestamp getCreatedAt();
}
