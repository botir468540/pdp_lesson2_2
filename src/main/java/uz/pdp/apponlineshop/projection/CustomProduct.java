package uz.pdp.apponlineshop.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.apponlineshop.entity.Attachment;
import uz.pdp.apponlineshop.entity.Product;
import uz.pdp.apponlineshop.entity.Property;

import java.util.List;

@Projection(types = Product.class)
public interface CustomProduct {
    Integer getId();

    String getName();

    Double getPrice();

    String getCategory();

    Attachment getPhoto();

    Integer getWarrantyYear();

    String getDescription();

    List<Property> getProperty();
}
