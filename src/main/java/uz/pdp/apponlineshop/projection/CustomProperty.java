package uz.pdp.apponlineshop.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.apponlineshop.entity.Property;

@Projection(types = Property.class)
public interface CustomProperty {
    Integer getId();

    String getValue();

    CustomCharacteristic getCharacteristic();
}
