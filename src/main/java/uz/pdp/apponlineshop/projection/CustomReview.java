package uz.pdp.apponlineshop.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.apponlineshop.entity.Product;
import uz.pdp.apponlineshop.entity.Review;
import uz.pdp.apponlineshop.entity.User;

import java.sql.Date;

@Projection(types = Review.class)
public interface CustomReview {
    Integer getId();

    String getComment();

    User getUser();

    Double getRating();

    Product getProduct();

    Date getDate();
}
