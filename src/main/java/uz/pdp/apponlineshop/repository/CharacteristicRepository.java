package uz.pdp.apponlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.apponlineshop.entity.Characteristic;
import uz.pdp.apponlineshop.projection.CustomCharacteristic;

@RepositoryRestResource(path = "characteristic",collectionResourceRel = "list",excerptProjection = CustomCharacteristic.class)
public interface CharacteristicRepository extends JpaRepository<Characteristic,Integer> {


}
