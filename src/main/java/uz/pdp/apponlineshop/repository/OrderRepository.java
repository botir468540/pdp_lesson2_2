package uz.pdp.apponlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.apponlineshop.entity.Order;
import uz.pdp.apponlineshop.projection.CustomOrder;

@RepositoryRestResource(path = "order",collectionResourceRel = "list",excerptProjection = CustomOrder.class)
public interface OrderRepository extends JpaRepository<Order,Integer> {

}
